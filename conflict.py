from pathlib import Path
from ical.calendar_stream import IcsCalendarStream
from ical.exceptions import CalendarParseError

filename = Path("./dmz.ics")
with filename.open() as ics_file:
    try:
        calendar = IcsCalendarStream.calendar_from_ics(ics_file.read())
    except CalendarParseError as err:
        print(f"Failed to parse ics file '{str(filename)}': {err}")
    else:
        print([event.summary for event in calendar.timeline])